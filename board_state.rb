module AntOfWar
  class BoardState
    def initialize(board_data)
      @board_data = board_data
    end

    def ants_of_player(player_id)
      @board_data["ants"].select { |h| h["player"] == player_id }
    end

    def player_turn
      @board_data["game"]["next_player"].to_i
    end
  end
end
