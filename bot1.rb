require_relative './game_socket'
require_relative './random_strategy'
require_relative './board_state'

strategy = AntOfWar::RandomStrategy.new

AntOfWar::GameSocket.new.connect do |player_id, game_state|
  board_state = AntOfWar::BoardState.new(game_state)
  strategy.build_commands(player_id, board_state)
end
