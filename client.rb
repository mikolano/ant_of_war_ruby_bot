module AntOfWar
  class Client
    def spawn_command(player_id)
      {cmd: "spawn", player_id: player_id}
    end

    def move_command(x, y, ant_id)
      {cmd: "move", id: ant_id, to: [x, y]}
    end
  end
end
