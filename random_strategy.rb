require_relative './client'

module AntOfWar
  class RandomStrategy
    def initialize(client: AntOfWar::Client.new)
      @client = client
    end

    def build_commands(player_id, board_state)
      if board_state.player_turn == player_id
        ants = board_state.ants_of_player(player_id)

        commands = []
        commands << client.spawn_command(player_id)
        commands += ants.map { |ant| random_move_command(ant) }
      else
        []
      end
    end

    private

    attr_reader :commands, :client

    def random_move_command(ant)
      x, y = rand_xy
      client.move_command(ant["x"] + x, ant["y"] + y, ant["id"])
    end

    def rand_xy
      y = rand(3) - 1
      x = rand(3) - 1
      x == 0 && y == 0 ? rand_xy : [x,y]
    end
  end
end
