require 'logger'
require 'faye/websocket'
require 'json'
require 'eventmachine'

module AntOfWar
  class GameSocket
    def initialize(logger: Logger.new(STDOUT))
      @logger = logger
      @player_id = nil
    end

    def connect(&block)
      EM.run do
        ws = Faye::WebSocket::Client.new('ws://0.0.0.0:4000/socket/websocket?vsn=1.0.0')

        ws.on(:open) do |event|
          logger.info('AntOfWar::Client --- Connecting')
          ws.send(JSON.dump(join_message))
        end

        ws.on(:message) do |event|
          message = JSON.parse(event.data)
          case message["event"]
          when "hello_message"
            handle_hello_message(message["payload"])
          when "board_state"
            logger.info("AntOfWar::Client --- Game tick")
            commands = yield player_id, message["payload"]
            ws.send(JSON.dump(commands_message(commands))) unless commands.empty?
          end
        end

        ws.on :error do |event|
          logger.error("AntOfWar::Client --- Failed! #{event.inspect}")
        end

        ws.on :close do |event|
          logger.info("AntOfWar::Client --- Closing connection: #{event.code} #{event.reason}")
          ws = nil
        end
      end
    end

    private
    attr_reader :player_id, :logger, :command_buffer

    def handle_hello_message(payload)
      @player_id = payload["body"]["player_id"].to_i
      logger.info("AntOfWar::Client --- Connected! Player id: #{@player_id}")
    end

    def join_message
      {topic: "observer:lobby", event: "phx_join", payload: {}, ref: "1"}
    end

    def commands_message(commands)
      {topic: "observer:lobby", event: "user_command", payload: commands, ref: "1"}
    end
  end
end
